import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    fighterElement.append(createFighterInfo(fighter, position));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo(fighter, position) {
  const { name, health, attack, defense, source } = fighter;
  const fighterData = [
    { text: 'Name', value: name },
    { text: 'Health', value: health },
    { text: 'Attack', value: attack },
    { text: 'Defense', value: defense },
  ];
  const positionClassName = position === 'right' ? 'arena___right-fighter' : '';
  const infoElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });
  const imgElement = createElement({
    tagName: 'img',
    attributes: { src: source, title: name, alt: name, style: 'max-height: 35vh' },
  });
  const dataElements = fighterData.map((fighter) => {
    const element = createElement({
      tagName: 'div',
      className: 'arena___fighter-name',
    });
    element.innerText = `${fighter.text}: ${fighter.value}`;
    return element;
  });
  infoElement.append(imgElement);
  infoElement.append(...dataElements);
  return infoElement;
}
